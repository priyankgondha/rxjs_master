import { Component, OnInit } from '@angular/core';
import { from, of } from 'rxjs';
import { map, mergeAll, mergeMap } from 'rxjs/operators';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-mergemap',
  templateUrl: './mergemap.component.html',
  styleUrls: ['./mergemap.component.css']
})
export class MergemapComponent implements OnInit {

  data2;
  constructor(private ds: DesignUtilityService) { }

  getData(data) {
    return of(data + 'Video Uploaded')
  }
  ngOnInit(): void {
    const source = from(['Tech', 'Comedy', 'News']);

    // Ex - 01 | Map
    source.pipe(map(res => this.getData(res))
    )
      .subscribe(res => {
        console.log(res);
        this.data2 = res;
        this.ds.print(this.data2, 'elContainer');
      })

    // Ex - 02 | Map + MergeAll
    source.pipe(
      map(res => this.getData(res)),
      mergeAll())
      .subscribe(res => {
        console.log(res);
        this.data2 = res;
        this.ds.print(this.data2, 'elContainer2');
      })


    // Ex - 03 | MergeMap
    source.pipe(
      mergeMap(res => this.getData(res)))
      .subscribe(res => {
        console.log(res);
        this.data2 = res;
        this.ds.print(this.data2, 'elContainer3');
      })

  }

}
