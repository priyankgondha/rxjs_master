import { Component, OnInit } from '@angular/core';
import { interval, Subscription, timer } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-interval',
  templateUrl: './interval.component.html',
  styleUrls: ['./interval.component.css']
})
export class IntervalComponent implements OnInit {

  obsmsg;
  videosubscription: Subscription;
  constructor(private ds: DesignUtilityService) { }

  ngOnInit(): void {

    // const broadcastvideo = interval(1000);
    const broadcastvideo = timer(2000, 1000); //timer(delay,interval)


    this.videosubscription = broadcastvideo.subscribe(res => {
      console.log(res);
      this.obsmsg = 'video ' + res;
      this.ds.print(this.obsmsg, 'elContainer')
      this.ds.print(this.obsmsg, 'elContainer2')

      if (res == 5) {
        this.videosubscription.unsubscribe();
      }
    })
  }

}
