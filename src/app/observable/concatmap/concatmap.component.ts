import { Component, OnInit } from '@angular/core';
import { from, of } from 'rxjs';
import { map, mergeAll, mergeMap, concatAll, delay, concatMap } from 'rxjs/operators';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-concatmap',
  templateUrl: './concatmap.component.html',
  styleUrls: ['./concatmap.component.css']
})
export class ConcatmapComponent implements OnInit {

  constructor(private ds: DesignUtilityService) { }
  data2: any;
  getData(data) {
    return of(data + 'Video Uploaded').pipe(delay(2000))
  }
  ngOnInit(): void {

    const source = from(['Tech', 'Comedy', 'News']);

    // Ex - 01 | Map
    source.pipe(map(res => this.getData(res))
    )
      .subscribe(res => res.subscribe(res2 => {
        console.log(res2);
        this.ds.print(res2, 'elContainer');
      }))

    // Ex - 02 | Map + ConcatAll
    source.pipe(map(res => this.getData(res)),
      concatAll()
    )
      .subscribe(res => {
        console.log(res);
        this.ds.print(res, 'elContainer2');
      })

    // Ex - 03 | ConcatMap
    source.pipe(
      concatMap(res => this.getData(res)))
      .subscribe(res => {
        console.log(res);
        this.data2 = res;
        this.ds.print(this.data2, 'elContainer3');
      })

  }

}
