import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})
export class CustomComponent implements OnInit, OnDestroy {

  techStatus;
  techStatus2;
  names;
  nameStauts;
  subs2: Subscription;
  constructor(private ds: DesignUtilityService) { }


  ngOnInit(): void {

    // Ex 1(Manual)
    const cusObj1 = Observable.create(observer => {

      setTimeout(() => {
        observer.next('Angular');
      }, 1000)
      setTimeout(() => {
        observer.next('Typescript');
      }, 2000)
      setTimeout(() => {
        observer.next('Html');
      }, 3000)
      setTimeout(() => {
        observer.next('Java');
        // observer.error(new Error('Limit Excced'))
        // this.techStatus = 'error'
      }, 4000)
      setTimeout(() => {
        observer.next('JS');
        this.techStatus = 'completed'
      }, 5000)

    })

    cusObj1.subscribe(res => {
      // console.log(res);
      this.ds.print(res, 'elContainer')
    }, (err) => {
      this.techStatus = 'error';
    },
      () => {
        this.techStatus = 'completed';
      }
    )

    // Ex 2(Custom Interval)

    const arr2 = ['Angular', 'Typescript', 'Html & CSS', 'JS']
    const cusObj2 = Observable.create(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(arr2[count]);

        if (count >= 3) {
          observer.error()
        }
        if (count >= 5) {
          observer.complete()
        }
        count++;
      }, 1000)
    })

    this.subs2 = cusObj2.subscribe(res => {
      // console.log("obj2", res);
      this.ds.print(res, 'elContainer2')
    },
      (err) => {
        this.techStatus = 'error';
      },
      () => {
        this.techStatus = 'completed';
      })

    // Ex 3(Random Name)

    const arr3 = ['Anup', 'Priyank', 'Karan', 'Alex', 'John']
    const cusObj3 = Observable.create(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(arr3[count]);

        if (count >= 3) {
          observer.error("Limit error")
        }
        if (count >= 5) {
          observer.complete()
        }
        count++;
      }, 1000)
    })

    cusObj3.subscribe(res => {
      this.names = res;
    },
      (err) => {
        this.nameStauts = 'error';
      },
      () => {
        this.nameStauts = 'completed';
      })


  }
  ngOnDestroy() {
    this.subs2.unsubscribe();
  }

}
