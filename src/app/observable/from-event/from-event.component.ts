import { AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-from-event',
  templateUrl: './from-event.component.html',
  styleUrls: ['./from-event.component.css']
})
export class FromEventComponent implements OnInit, AfterViewInit {

  constructor() { }
  @ViewChild('addBtn') addBtn: ElementRef;

  ngOnInit(): void {

  }
  ngAfterViewInit() {
    let count = 1;
    fromEvent(this.addBtn.nativeElement, 'click').subscribe(res => {
      let countval = 'Video ' + count++;
      console.log(countval);
      this.print(countval);
    })
  }

  print(val) {
    let el = document.createElement('li');
    el.innerText = val;
    document.getElementById('elContainer')?.appendChild(el);
  }
}
