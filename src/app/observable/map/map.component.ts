import { Component, OnInit } from '@angular/core';
import { interval, map, Subscription, from } from 'rxjs';
import { DesignUtilityService } from 'src/app/design-utility.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(private ds: DesignUtilityService) { }
  sub1: Subscription;
  sub2: Subscription;

  msg1: any;
  msg2: any;
  msg3: any;

  ngOnInit(): void {

    // Ex-1
    const broadCastVideo = interval(1000);
    this.sub1 = broadCastVideo.pipe(map(data => 'video ' + (data + 1)))
      .subscribe(res => {
        this.msg1 = res;
        // console.log('videos ' + res); 
        console.log('message this 1:' + this.msg1);
      })

    setTimeout(() => {
      this.sub1.unsubscribe()
    }, 5000)

    // Ex-2
    const broadCastVideo2 = interval(1000);
    this.sub2 = broadCastVideo2.pipe(map(data => (data + 1) * 3))
      .subscribe(res => {
        this.msg2 = res;
        console.log('message this 2:' + this.msg2);
      })

    setTimeout(() => {
      this.sub2.unsubscribe()
    }, 5000)

    // Ex-3
    const members = from([
      { id: 1, name: 'Priyank' },
      { id: 2, name: 'Karan' },
      { id: 3, name: 'Krushik' },
      { id: 4, name: 'Savan' },
      { id: 5, name: 'Utam' }
    ])

    members.pipe(map(data => data.name)).subscribe(res => {
      // console.log(res);
      this.ds.print(res, 'elContainer');
    })

  }

}
